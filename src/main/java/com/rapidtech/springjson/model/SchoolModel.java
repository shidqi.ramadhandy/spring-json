package com.rapidtech.springjson.model;

import lombok.Data;

@Data
public class School {
    private String title;
    private String name;
    private String level;
}
